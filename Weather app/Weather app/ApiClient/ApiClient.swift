//
//  ApiClient.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

class APIClient {
    static func getWeather (city: String, days: Int, success:@escaping (Forecast) -> (), error:@escaping (WebServiceManager.WSError) -> ()) throws {
        
        let request = GetWeatherRequest.init(city: city, days: String(days))
        
        do {
            let urlRequest = try request.asURLRequest()
            
            WebServiceManager.shared.performRequest(request: urlRequest, success: { response in
                
                if let response = response {
                    do {
                        let forecast = try JSONDecoder().decode(Forecast.self, from: response)
                    
                        success(forecast)
                    }
                    catch (_) {
                        error(.apiError)
                    }
                }

            }) { (wsError) in
                error(wsError)
            }
        }
        catch (let error) {
            throw(error)
        }
    }
}
