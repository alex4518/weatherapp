//
//  GetWeatherRequest.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class GetWeatherRequest: BaseRequest {

    var city: String!
    var days: String!
    
    override var paramsDictionary: [String : String] {
        
        get {
            return self.params
        }
        
        set {
            self.paramsDictionary = self.params
        }
    }
    
    var params: [String:String] {
        
        var requestParameters = [String:String]()
        
        requestParameters["q"] = city
        requestParameters["num_of_days"] = days

        return requestParameters
    }
    
    init(city: String, days: String) {
        super.init()
        
        self.requestMethod = .get
        
        self.city = city
        self.days = days
    }
}
