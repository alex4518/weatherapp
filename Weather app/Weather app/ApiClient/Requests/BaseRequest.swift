//
//  BaseRequest.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class BaseRequest: NSObject {
    enum WSRequestMethod : String {
        case get = "GET"
        case post = "POST"
    }
    
    private let baseUrl = "https://api.worldweatheronline.com/premium/v1/weather.ashx?"
    private let apiKey = "773d9d0ec21a4880a37134933190804"
    
    var paramsDictionary = [String:String]()
    var requestMethod: WSRequestMethod!
    
    var paramsString:String {
        
        var paramsString = ""
        
        for key in self.paramsDictionary.keys {
            
            paramsString.append("&")
            paramsString.append(key)
            paramsString.append("=")
            paramsString.append(self.paramsDictionary[key]!)
        }
        
        return paramsString
    }
    
    
    var url:URL! {
        
        guard let params = self.paramsString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return URL.init(string: "") }
        
        let strUrl = (self.baseUrl + "key=" + self.apiKey + "&format=json" + params)
        
        let url = URL.init(string: strUrl)
        
        return url!
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: self.url)
        
        // HTTP Method
        urlRequest.httpMethod = self.requestMethod.rawValue
        
        
        // Parameters
        if urlRequest.httpMethod != "GET" {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: self.paramsDictionary, options: [])
            } catch {
                throw WebServiceManager.WSError.malformedRequest
            }
        }
        
        return urlRequest
    }
}
