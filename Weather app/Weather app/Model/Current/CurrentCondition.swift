//
//  CurrentCondition.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct CurentCondition: Codable {
    var FeelsLikeC: String?
    var FeelsLikeF: String?
    var cloudcover: String?
    var humidity: String?
    var observation_time: String?
    var precipMM: String?
    var pressure: String?
    var temp_C: String?
    var temp_F: String?
    var uvIndex: Int?
    var visibility: String?
    var weatherCode: String?
    var weatherDesc: [WeatherDescription]?
    var weatherIconUrl: [WeatherIcon]?
    var winddir16Point: String?
    var winddirDegree: String?
    var windspeedKmph: String?
    var windspeedMiles: String?
    
    func getDisplayTempC() -> String {
        return "\(self.temp_C ?? "") °C"
    }
    
    func getDisplayFeelsLikeC() -> String {
        return "Feels like \(self.FeelsLikeC ?? "") °C"
    }
    
    func getDisplayWindKm() -> String {
        return "Wind \(self.windspeedKmph ?? "") Km/h"
    }
    
    func getDisplayHumidity() -> String {
        return "Humidity \n \(self.humidity ?? "")"
    }
    
    func getDisplayVisibility() -> String {
        return "Visibility \n \(self.visibility ?? "")"
    }
}
