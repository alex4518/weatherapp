//
//  WeatherIcon.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct WeatherIcon: Codable {
    var value: String?
}
