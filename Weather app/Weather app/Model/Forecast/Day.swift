//
//  Day.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct Day: Codable {
    var date: String?
    var astronomy: [Astronomy]?
    var maxtempC: String?
    var maxtempF: String?
    var mintempC: String?
    var mintempF: String?
    var totalSnow_cm: String?
    var sunHour: String?
    var uvIndex: String?
    var hourly: [HourlyForecast]?
    
    func getDisplayMinTempC() -> String {
        return "Min \(self.mintempC ?? "")°"
    }
    
    func getDisplayMaxTempC() -> String {
        return "Max \(self.maxtempC ?? "")°"
    }
    
    func getDisplayTempRange() -> String {
        return "\(self.mintempC ?? "")° - \(self.maxtempC ?? "")°"
    }
    
    func getDayName() -> String {
        guard self.date != nil else {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        
        guard let date = dateFormatter.date(from: self.date!) else {
            return ""
        }
        
        dateFormatter.dateFormat = "EEEE"

        
        return dateFormatter.string(from: date)
    }

}
