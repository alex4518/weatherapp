//
//  Request.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct Request: Codable {
    var type: String?
    var query: String?
}
