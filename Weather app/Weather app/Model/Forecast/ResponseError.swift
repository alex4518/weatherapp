//
//  ResponseError.swift
//  Weather app
//
//  Created by Alexandros Albanis on 20/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct ResponseError: Codable {
    var msg: String?
}
