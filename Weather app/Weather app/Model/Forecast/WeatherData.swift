//
//  WeatherData.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct WeatherData: Codable { 
    var ClimateAverages: [Month]?
    var current_condition: [CurentCondition]?
    var weather: [Day]?
    var request: [Request]?
    var error: [ResponseError]?
    
    func hasError() -> Bool {
        return error?.first?.msg != nil
    }
    
    func getErrorMsg() -> String {
        return (error?.first!.msg)! //these vars can be force unwrapped as we have checked their value in the above function
    }
}
