//
//  City.swift
//  Weather app
//
//  Created by Alexandros Albanis on 11/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct City: Equatable {
    var name: String
    var country: String
    var selected: Bool
    var isCurrentLocation: Bool
    
    static func ==(lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name && lhs.country == rhs.country && lhs.isCurrentLocation == rhs.isCurrentLocation
    }
}
