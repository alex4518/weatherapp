//
//  Hour.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct HourlyForecast: Codable {
    var time: String?
    var tempC: String?
    var tempF: String?
    var windspeedMiles: String?
    var windspeedKmph: String?
    var winddirDegree: String?
    var winddir16Point: String?
    var weatherCode: String?
    var weatherIconUrl: [WeatherIcon]?
    var weatherDesc: [WeatherDescription]?
    var precipMM: String?
    var humidity: String?
    var visibility: String?
    var pressure: String?
    var cloudcover: String?
    var HeatIndexC: String?
    var HeatIndexF: String?
    var DewPointC: String?
    var DewPointF: String?
    var WindChillC: String?
    var WindChillF: String?
    var WindGustMiles: String?
    var WindGustKmph: String?
    var FeelsLikeC: String?
    var FeelsLikeF: String?
    var chanceofrain: String?
    var chanceofremdry: String?
    var chanceofwindy: String?
    var chanceofovercast: String?
    var chanceofsunshine: String?
    var chanceoffrost: String?
    var chanceofhightemp: String?
    var chanceoffog: String?
    var chanceofsnow: String?
    var chanceofthunder: String?
    var uvIndex: String?
    
    func getDisplayTempC() -> String {
        return "\(self.tempC ?? "") °"
    }
    
    func getDisplayTime() -> String {
        if self.time == "0" {
            return "00:00"
        }
        guard self.time != nil else  {
            return ""
        }
        guard self.time!.count > 2 else {
            return ""
        }
        
        let hours = (self.time!.count > 3 ? self.time?.prefix(2) : self.time?.prefix(1))! //get first or two first digits depending on the time
        let minutes = self.time!.suffix(2) //get last two digits
        
        guard let intHours = Int(hours) else { return "" }
        guard let intMinutes = Int(minutes) else { return "" }
        
        return String(format: "%02d:%02d", intHours, intMinutes)
    }
    
    func getDisplayFeelsLikeC() -> String {
        return "Feels like \(self.FeelsLikeC ?? "") °C"
    }
    
    func getDisplayWindKm() -> String {
        return "Wind \(self.windspeedKmph ?? "") Km/h"
    }
    
    func getDisplayHumidity() -> String {
        return "Humidity \n \(self.humidity ?? "")"
    }
    
    func getDisplayVisibility() -> String {
        return "Visibility \n \(self.visibility ?? "")"
    }
    
    func isCurrentHour() -> Bool {

        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        
        let currentDateString = dateFormatter.string(from: currentDate)
        let formattedCurrentDate = dateFormatter.date(from: currentDateString)
        
        guard formattedCurrentDate != nil else {
            return false
        }
        
            
        if let date = self.getDateFromTime(time: self.getDisplayTime()) {
            
            let intervalToNow = currentDate.timeIntervalSince(date)
            
            if intervalToNow < 10800 && intervalToNow > 0 { // 3 hours in seconds
                return true
            }
            
        }
        
        return false
        
    }
    
    //returns a date object with today's date and a specified hour
    func getDateFromTime(time: String) -> Date? {
        let components = time.split(separator: ":").map(String.init) // split hours and minutes and convert to string
        
        guard components.count == 2 else {
            return nil //wrong time format
        }
        
        guard let hours = Int(components.first!) else { return nil }
        guard let minutes = Int(components[1]) else { return nil }
        
        let date = Calendar.current.date(bySettingHour: hours , minute: minutes, second: 0, of: Date())! //create a date with current day and the specified hour
        
        return date
    }
    
}
