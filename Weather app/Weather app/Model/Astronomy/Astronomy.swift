//
//  Astronomy.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct Astronomy: Codable {
    var sunrise: String?
    var sunset: String?
    var moonrise: String?
    var moonset: String?
    var moon_phase: String?
    var moon_illumination: String?
}
