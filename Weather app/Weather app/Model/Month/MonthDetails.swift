//
//  MonthDetails.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct MonthDetails: Codable {
    var absMaxTemp: String?
    var absMaxTemp_F: String?
    var avgDailyRainfall: String?
    var avgMinTemp: String?
    var avgMinTemp_F: String?
    var index: String?
    var name: String?
}
