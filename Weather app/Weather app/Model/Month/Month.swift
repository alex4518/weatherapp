//
//  Month.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

struct Month: Codable {
    var month: [MonthDetails]?
}
