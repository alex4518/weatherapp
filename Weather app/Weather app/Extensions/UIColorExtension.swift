//
//  UIColorExtension.swift
//  Weather app
//
//  Created by Alexandros Albanis on 20/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    static let cityCellBackground = UIColor.init(red: 64, green: 63, blue: 63, alpha: 0.7)
    static let citiesViewBackground = UIColor.init(red: 30, green: 30, blue: 30, alpha: 1)
    static let weatherViewBackground = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let weatherCellBackground = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let weatherViewControllerBackground = UIColor.init(red: 51, green: 153, blue: 255, alpha: 1)
    static let weatherCellHeaderBackground = UIColor.init(red: 255, green: 255, blue: 255, alpha: 0.3)
    
}
