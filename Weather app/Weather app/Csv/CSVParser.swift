//
//  CSVParser.swift
//  Weather app
//
//  Created by Alexandros Albanis on 10/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class CSVParser: NSObject {

    func readDataFromCSV(fileName:String, fileType: String)-> String!{
        guard let filepath = Bundle.main.path(forResource: fileName, ofType: fileType)
            else {
                return nil
        }
        do {
            let contents = try String(contentsOfFile: filepath, encoding: .utf8)
            return contents
        } catch {
            print("File Read Error for file \(filepath)")
            return nil
        }
    }
    
    func parseData(data: String) -> [City] {
        let parsedData: [City] = data
            .components(separatedBy: "\n") //get one line
            .map({
                let components = $0.components(separatedBy: ",")
                let city = City.init(name: components[0], country: components[1], selected: false, isCurrentLocation: false)
                return city;
                
            })
        
        return parsedData;
    }
}
