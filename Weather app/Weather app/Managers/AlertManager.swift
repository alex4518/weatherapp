//
//  AlertManager.swift
//  Weather app
//
//  Created by Alexandros Albanis on 12/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class AlertManager: NSObject {
    
    static var shared = AlertManager()
    
    private override init() {}
    
    
    func showAlert(title: String?, message: String, buttonTitle: String, vc: UIViewController, handler:((UIAlertAction) -> ())?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: handler))
        vc.present(alert, animated: true, completion: nil)
    }
}
