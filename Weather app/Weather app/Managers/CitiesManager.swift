//
//  CitiesManager.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import CoreData

class CitiesManager: NSObject {

    static var shared = CitiesManager()
    
    private override init() {}
    
    func loadCitiesFromCoreData() -> [City]? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
        request.returnsObjectsAsFaults = false
        
        var cities = [City]()
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let name = data.value(forKey: "name") as? String, let country = data.value(forKey: "country") as? String, let selected =  data.value(forKey: "selected") as? Bool, let isCurrentLocation  = data.value(forKey: "isCurrentLocation") as? Bool {
                    
                    let city = City.init(name: name, country: country, selected: selected, isCurrentLocation: isCurrentLocation)
                    cities.append(city)
                }
            }
            
        } catch {
            
            return nil;
        }
        
        return cities
    }
    
    func saveToCoreData(city: City) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let name = data.value(forKey: "name") as? String, let country = data.value(forKey: "country") as? String, let selected =  data.value(forKey: "selected") as? Bool, let isCurrentLocation = data.value(forKey: "isCurrentLocation") as? Bool {
                    
                    data.setValue(false, forKey: "selected") //reset so the newly added city would become the selected one
                    let storedCity = City.init(name: name, country: country, selected: selected, isCurrentLocation: isCurrentLocation)
                    if city == storedCity { //check for duplicates before saving
                        return
                    }
                }
            }
            
        } catch {
            //no need for error handling at this point
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "Cities", in: context)
        let newCity = NSManagedObject(entity: entity!, insertInto: context)
        
        newCity.setValue(city.name, forKey: "name")
        newCity.setValue(city.country, forKey: "country")
        newCity.setValue(true, forKey: "selected")
        newCity.setValue(false, forKey: "isCurrentLocation")

        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func replaceCurrentLocationCity(with city:City) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let name = data.value(forKey: "name") as? String, let country = data.value(forKey: "country") as? String, let selected =  data.value(forKey: "selected") as? Bool, let isCurrentLocation = data.value(forKey: "isCurrentLocation") as? Bool {
                    
                    let storedCity = City.init(name: name, country: country, selected: selected, isCurrentLocation: isCurrentLocation)
                    if storedCity.isCurrentLocation {
                        
                        _ = self.remove(city: storedCity) //remove previous location
                    }
                }
            }
            
            let entity = NSEntityDescription.entity(forEntityName: "Cities", in: context)
            let newCity = NSManagedObject(entity: entity!, insertInto: context)
            
            newCity.setValue(city.name, forKey: "name")
            newCity.setValue(city.country, forKey: "country")
            newCity.setValue(result.count == 0 ? true : false, forKey: "selected") //set selected if it is the only city
            newCity.setValue(true, forKey: "isCurrentLocation")
            
        } catch {
            //no need for error handling at this point
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func remove(city: City) -> (Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request)
            for (coreDataCity) in result as! [NSManagedObject] {
                
                if let name = coreDataCity.value(forKey: "name") as? String, let country = coreDataCity.value(forKey: "country") as? String, let selected = coreDataCity.value(forKey: "selected") as? Bool, let isCurrentLocation = coreDataCity.value(forKey: "isCurrentLocation") as? Bool {
                    
                    let storedCity = City.init(name: name, country: country, selected: selected, isCurrentLocation: isCurrentLocation)
                    if city == storedCity {
                        context.delete(coreDataCity)

                        do {
                            try context.save()
                            return true
                        } catch {
                            return false
                        }
                    }
                }
            }
            
        } catch {
            return false
        }
        return false
    }
}
