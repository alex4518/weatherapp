//
//  WebServiceManager.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import Alamofire

class WebServiceManager: NSObject {
    
    enum WSError: Error {
        case apiError
        case responseError
        case timeoutError
        case noNetworkError
        case malformedRequest
    }
    
    static var shared = WebServiceManager()
    
    private override init() {}
    
    func performRequest(request: URLRequest, success:@escaping (Data?) -> (), serviceError:@escaping (WSError) -> ()) {
        AF.request(request).response { response in
            
            switch response.result {
            case .success(let res):
                success(res)
            case .failure(let error):
                let wsError = error.asAFError
                
                switch wsError?.responseCode {
                    case NSURLErrorTimedOut:
                        serviceError(.timeoutError)
                    case NSURLErrorNotConnectedToInternet:
                        serviceError(.noNetworkError)
                    default:
                        serviceError(.apiError)
                    }
                }
            
        }
    }
}
