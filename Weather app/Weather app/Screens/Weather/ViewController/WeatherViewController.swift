//
//  WeatherViewController.swift
//  Weather app
//
//  Created by Alexandros Albanis on 13/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import CoreLocation
import SVProgressHUD

class WeatherViewController: UIViewController {
    
    //MARK: Gesture transition direction
    enum TransitionDirection {
        case left
        case right
    }
    
    //MARK: constraints constants
    
    var pageControllBottom: CGFloat!
    var sectionHeaderLabelFont: CGFloat!
    var sectionHeaderLabelTop: CGFloat!
    var sectionHeaderLabelLeft: CGFloat!
    var sectionHeaderLabelRight: CGFloat!
    var sectionHeaderLabelBottom: CGFloat!

    private var isIpad = false

    //MARK: Table view
    private var tableView = UITableView()
    private var tableHeaderView: WeatherTableHeaderView!
    var cities = [City]()
    private var forecasts = [Int : Forecast]()
    
    private var hourlyForecast = [HourlyForecast]()
    private var dailyForecast = [Day]()
    
    private var selectedCityIndex = 0
    
    private var todaySelectedIndex = -1 //nothing selected
    private var longTermSelectedIndex = 0
    
    private var todayCellContentOffset: CGPoint?
    private var todayCellSelectedByUser = false //used to check if the user has ever selected any view of today cell
    
    //MARK: Location
    private let locationManager = CLLocationManager()
    
    //MARK: control components
    private var refreshControll = UIRefreshControl()
    private var pageControll = UIPageControl()

    //MARK: View lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isIpad = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.regular
        
        self.setConstants()
        
        self.tableHeaderView = WeatherTableHeaderView(isIpad: self.isIpad)
        
        self.view.backgroundColor = UIColor.weatherViewControllerBackground

        self.addPageControll()
        self.addTransitionGesture()
        self.addCitySelectionButton()
        self.addTableView()
        self.setTableViewHeader()
        
        self.tableView.sizeHeader()
        
        self.addPullToRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadCities()
    }
    
    //MARK: Constants
    
    private func setConstants() {
        self.pageControllBottom = self.isIpad ? -30 : -20
        self.sectionHeaderLabelFont = self.isIpad ? 24 : 16
        self.sectionHeaderLabelTop = self.isIpad ? 7 : 5
        self.sectionHeaderLabelLeft = self.isIpad ? 15: 10
        self.sectionHeaderLabelRight = self.isIpad ? -15 : -10
            self.sectionHeaderLabelBottom = self.isIpad ? -7 : -5
    }
    
    //MARK: Navigation items
    
    private func addCitySelectionButton() {
        let button = UIBarButtonItem(image: UIImage.init(named: "globe"), style: .plain, target: self, action: #selector(citiesButtonPressed(sender:)))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc private func citiesButtonPressed(sender: UIBarButtonItem) {
        self.navigationController?.pushViewController(MyCitiesViewController(), animated: true);
    }
    
    //MARK: Location
    
    private func detectLocation() {
        self.locationManager.delegate = self
        // Ask for background authorization.
        self.locationManager.requestAlwaysAuthorization()
        
        // Ask for foreground authorization.
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() && (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .notDetermined) {
            
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
            SVProgressHUD.show()
        }
        else if self.cities.count == 0 { // no cities added and location services disable
            self.navigationController?.setViewControllers([MyCitiesViewController(), SelectCityViewController()], animated: true)
        }
    }
    
    //MARK: Api call
    private func getWeather(location: String?, index: Int ) {
        guard location != nil else {
            return
        }
        do {
            SVProgressHUD.show()
            try APIClient.getWeather(city: location!, days: 5, success: { response in
                
                SVProgressHUD.dismiss()
                
                self.refreshControll.endRefreshing()
                
                self.forecasts[index] = response
                
                if (self.forecasts.count > self.self.selectedCityIndex) {
                    self.setupTable()
                }
            
            }, error: { error in
                
                SVProgressHUD.dismiss()
                
                var errorMessage = ""
                switch(error) {
                case .noNetworkError:
                    errorMessage = "No internet connection found. Please connect to the internet."
                case .apiError:
                    fallthrough
                case .timeoutError:
                    fallthrough
                case .malformedRequest:
                    fallthrough
                default:
                    errorMessage = "Something went wrong. Please try again later"
                }
                
                AlertManager.shared.showAlert(title: nil, message: errorMessage, buttonTitle: "Ok", vc: self, handler: nil)
            })
        }
        catch (_) {
            SVProgressHUD.dismiss()
            
            AlertManager.shared.showAlert(title: nil, message: "Something went wrong. Please try again later", buttonTitle: "Ok", vc: self, handler: nil)
        }
    }
    
    //MARK: Data binding
    
    @objc private func loadCities() {
        self.forecasts.removeAll()
        
        if let cities = CitiesManager.shared.loadCitiesFromCoreData() {
            
            self.cities = cities
            
            self.pageControll.numberOfPages = cities.count
            
            for (index, city) in cities.enumerated() {
                
                if city.selected {
                    self.selectedCityIndex = index
                    self.pageControll.currentPage = index
                }
                self.getWeather(location: city.name, index: index)
            }
        }
        
        self.detectLocation()
    }
    
    private func setupTable() {
        let forecast = self.forecasts[self.selectedCityIndex]
        
        self.hourlyForecast = forecast?.data?.weather?.first?.hourly ?? []
        self.dailyForecast = forecast?.data?.weather ?? []
        
        self.setCity()
        
        self.tableView.reloadData()
        self.setupHeaderViewForCurrent()
        self.tableView.sizeHeader()
    }
    
    private func setupHeaderViewForCurrent() {
        let forecast = self.forecasts[self.selectedCityIndex]

        if let url = forecast?.data?.current_condition?.first?.weatherIconUrl?.first?.value {
            self.setWeatherImage(url: url)
        }
        let temp = forecast?.data?.current_condition?.first?.getDisplayTempC()
        self.setTempLabel(temp: temp)
        
        let feelsTemp = forecast?.data?.current_condition?.first?.getDisplayFeelsLikeC()
        self.setFeelLabel(temp: feelsTemp)
        
        let min = forecast?.data?.weather?.first?.getDisplayMinTempC()
        self.setMinTempLabel(min: min)
        
        let max = forecast?.data?.weather?.first?.getDisplayMaxTempC()
        self.setMaxTempLabel(max: max)
        
        let wind = forecast?.data?.current_condition?.first?.getDisplayWindKm()
        self.setWindLabel(wind: wind)
        
        let weatherDesc = forecast?.data?.current_condition?.first?.weatherDesc?.first?.value
        self.setDescLabel(desc: weatherDesc)
        
        let humidity = forecast?.data?.current_condition?.first?.getDisplayHumidity()
        self.setHumidityLabel(desc: humidity)
        
        let visibility = forecast?.data?.current_condition?.first?.getDisplayVisibility()
        self.setVisibilityLabel(desc: visibility)
    }
    
    private func setupHeaderViewForDay(day: Day, hourIndex: Int) {
        
        if let url = day.hourly?[hourIndex].weatherIconUrl?.first?.value {
            self.setWeatherImage(url: url)
        }
        let temp = day.hourly?[hourIndex].getDisplayTempC()
        self.setTempLabel(temp: temp)
        
        let feelsTemp = day.hourly?[hourIndex].getDisplayFeelsLikeC()
        self.setFeelLabel(temp: feelsTemp)
        
        let min = day.getDisplayMinTempC()
        self.setMinTempLabel(min: min)
        
        let max = day.getDisplayMaxTempC()
        self.setMaxTempLabel(max: max)
        
        let wind = day.hourly?[hourIndex].getDisplayWindKm()
        self.setWindLabel(wind: wind)
        
        let weatherDesc = day.hourly?[hourIndex].weatherDesc?.first?.value
        self.setDescLabel(desc: weatherDesc)
        
        let humidity = day.hourly?[hourIndex].getDisplayHumidity()
        self.setHumidityLabel(desc: humidity)
        
        let visibility = day.hourly?[hourIndex].getDisplayVisibility()
        self.setVisibilityLabel(desc: visibility)
    }
    
    private func setCity() {
        let city = self.cities[self.selectedCityIndex]
        self.tableHeaderView.cityLabel.text = city.name
        
        if !city.isCurrentLocation {
            self.tableHeaderView.locationIcon.isHidden = true
        }
        else {
            self.tableHeaderView.locationIcon.isHidden = false
        }
    }

    
    private func setWeatherImage(url: String) {
        self.tableHeaderView.weatherIcon.sd_setImage(with: URL(string: url))
    }
    private func setTempLabel(temp: String?) {
        self.tableHeaderView.tempLabel.text = temp
    }
    
    private func setFeelLabel(temp: String?) {
        self.tableHeaderView.feelsLikeLabel.text = temp
    }
    
    private func setMinTempLabel(min: String?) {
        self.tableHeaderView.minTempLabel.text = min
    }
    
    private func setMaxTempLabel(max: String?) {
        self.tableHeaderView.maxTempLabel.text = max
    }
    
    private func setWindLabel(wind: String?) {
        self.tableHeaderView.windLabel.text = wind
    }
    
    private func setDescLabel(desc: String?) {
        self.tableHeaderView.descLabel.text = desc
    }
    
    private func setHumidityLabel(desc: String?) {
        self.tableHeaderView.humidityLabel.text = desc
    }
    
    private func setVisibilityLabel(desc: String?) {
        self.tableHeaderView.visibilityLabel.text = desc
    }
}

//MARK UI
extension WeatherViewController {
    private func addTableView() {
        self.view.addSubview(self.tableView)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.tableView.register(WeatherTableViewCell.self, forCellReuseIdentifier: "weatherCell")
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension;
        self.tableView.estimatedSectionHeaderHeight = 50;
        
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    func setTableViewHeader() {
        self.tableView.tableHeaderView = self.tableHeaderView
        
        self.tableHeaderView.widthAnchor.constraint(equalTo: self.tableView.widthAnchor).isActive = true
        self.tableHeaderView.topAnchor.constraint(equalTo: self.tableView.topAnchor).isActive = true
        self.tableHeaderView.leftAnchor.constraint(equalTo: self.tableView.leftAnchor).isActive = true
    }
    
    private func addPageControll() {
        self.pageControll.translatesAutoresizingMaskIntoConstraints = false
        self.pageControll.pageIndicatorTintColor = UIColor.black
        self.pageControll.numberOfPages = 1
        self.view.addSubview(self.pageControll)
        
        self.pageControll.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.pageControll.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: self.pageControllBottom).isActive = true
    }
    
    private func addTransitionGesture() {
        
        let leftSwipeRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeHandler(sender:)))
        leftSwipeRecognizer.direction = .left
        self.tableView.addGestureRecognizer(leftSwipeRecognizer)
        
        let rightSwipeRecognizer = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeHandler(sender:)))
        rightSwipeRecognizer.direction = .right
        self.tableView.addGestureRecognizer(rightSwipeRecognizer)
    }
    
    @objc private func swipeHandler(sender:UISwipeGestureRecognizer) {
        
        
        if sender.direction == .left {
            if (self.forecasts.count > self.selectedCityIndex + 1) {
                
                self.selectedCityIndex += 1
                self.setupTable()
                
                self.showTransition(direction: .right)
            }
        }
        else {
            if (self.selectedCityIndex != 0) {
                
                self.selectedCityIndex -= 1
                self.setupTable()
                
                self.showTransition(direction: .left)
            }
        }
        
        self.pageControll.currentPage = self.selectedCityIndex
    }
    
    private func addPullToRefresh() {
        
        self.refreshControll.addTarget(self, action: #selector(loadCities), for: .valueChanged)
        
        self.tableView.addSubview(self.refreshControll)
    }
    
    //MARK: transition animation
    private func showTransition(direction: TransitionDirection) {
        
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.duration = 0.6
        transition.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.easeInEaseOut)
        if direction == .left {
            transition.subtype = CATransitionSubtype.fromLeft
        }
        else {
            transition.subtype = CATransitionSubtype.fromRight
        }
        
        self.tableView.layer.add(transition, forKey: nil)
    }
}

//actions
extension WeatherViewController {
    @objc private func weatherButtonPressed(sender: WeatherButton) {

        if sender.type == .today {
            
            self.todayCellSelectedByUser = true
            
            self.todaySelectedIndex = sender.index
            
            self.setupHeaderViewForDay(day: self.dailyForecast[self.longTermSelectedIndex], hourIndex: self.todaySelectedIndex)
            
            let cell = self.tableView.visibleCells.first as? WeatherTableViewCell
            self.todayCellContentOffset = cell?.scrollView.contentOffset
            
            self.tableView.reloadData()

        }
        else {
            
            self.hourlyForecast = self.forecasts[self.selectedCityIndex]?.data?.weather?[sender.index].hourly ?? []
            
            self.longTermSelectedIndex = sender.index
            self.todaySelectedIndex = 0
            
            self.setupHeaderViewForDay(day: self.dailyForecast[self.longTermSelectedIndex], hourIndex: 0)
            
            self.tableView.reloadData()
        }
        
    }
}

extension WeatherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension WeatherViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let weatherCell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as? WeatherTableViewCell
        
        //clear stack view
        for view in weatherCell?.stackView.arrangedSubviews ?? [] {
            view.removeFromSuperview()
        }

        var frame = CGRect.init(x: 0, y: 0, width: 0, height: 0)
        
        if indexPath.section == 0 { //short term forecast
            
            for (index, hourForecast) in self.hourlyForecast.enumerated() {
                let weatherButton = WeatherButton.init(isIpad: self.isIpad)
                weatherButton.addTarget(self, action: #selector(weatherButtonPressed(sender:)), for: .touchUpInside)
                weatherButton.icon.sd_setImage(with: URL(string: hourForecast.weatherIconUrl?.first?.value ?? ""))
                weatherButton.timeTextView.text = hourForecast.getDisplayTime()
                weatherButton.tempLabel.text = hourForecast.getDisplayTempC()
                weatherButton.index = index
                weatherButton.type = .today
                weatherButton.isSelected = self.todaySelectedIndex == -1 ? hourForecast.isCurrentHour() : (self.todaySelectedIndex == index)
                

                weatherCell?.stackView.addArrangedSubview(weatherButton)
                self.tableView.layoutIfNeeded()

                //scroll to the current time if it is not in the screen
                if weatherButton.isSelected {
                    
                    if !self.todayCellSelectedByUser {//user has not selected any of today cell's view
                        frame = weatherButton.frame //get frame of current weather view
                        
                        weatherCell?.scrollView.contentOffset = CGPoint.init(x: frame.origin.x, y: 0) // scroll to current weather view
                    }
                    else {
                        weatherCell?.scrollView.contentOffset = self.todayCellContentOffset ?? CGPoint.init(x: 0, y: 0)
                    }

                    self.todaySelectedIndex = index
                }
                
            }
            
        }
        else if indexPath.section == 1 { //long term forecast
            for (index, dayForecast) in self.dailyForecast.enumerated() {
                let weatherButton = WeatherButton.init(isIpad: self.isIpad)
                weatherButton.addTarget(self, action: #selector(weatherButtonPressed(sender:)), for: .touchUpInside)
                weatherButton.icon.sd_setImage(with: URL(string: dayForecast.hourly?.middle?.weatherIconUrl?.first?.value ?? "")) //get midday icon
                weatherButton.timeTextView.text = dayForecast.getDayName()
                weatherButton.tempLabel.text = dayForecast.getDisplayTempRange()
                weatherButton.index = index
                weatherButton.type = .longTerm
                weatherButton.isSelected = self.longTermSelectedIndex == weatherButton.index
                
                weatherCell?.stackView.addArrangedSubview(weatherButton)
            }
        }
        
        return weatherCell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        
        view.backgroundColor = UIColor.weatherCellHeaderBackground

        
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.textColor = UIColor.white
        title.font = UIFont.systemFont(ofSize: self.sectionHeaderLabelFont)
        
        view.addSubview(title)

        
        title.topAnchor.constraint(equalTo: view.topAnchor, constant: self.sectionHeaderLabelTop).isActive = true
        title.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: self.sectionHeaderLabelBottom).isActive = true
        title.leftAnchor.constraint(equalTo: view.leftAnchor, constant: self.sectionHeaderLabelLeft).isActive = true
        title.rightAnchor.constraint(equalTo: view.rightAnchor, constant: self.sectionHeaderLabelRight).isActive = true

        
        if section == 0 {
            title.text = "Today"
        } else if section == 1 {
            title.text = "Long Term"
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.frame = CGRect.init(x: 0, y: 0, width: self.tableView.frame.width, height: 20)
        
        return view
    }
}

extension UITableView {
    func sizeHeader() {
        if let header = self.tableHeaderView {
            
            header.setNeedsLayout()
            header.layoutIfNeeded()
            
            let height = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = header.frame
            frame.size.height = height
            header.frame = frame
            
            self.tableHeaderView = header
        }
    }
}

extension Array {
    
    var middle: Element? {
        guard count != 0 else { return nil }
        
        let middleIndex = (count > 1 ? count - 1 : count) / 2
        return self[middleIndex]
    }
    
}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let coordinates: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        //user reverse geolocation to convert coordinates to city
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler:
            {
                placemarks, error -> Void in

                // Place details
                guard let placeMark = placemarks?.first else { return }

                // City
                if let cityName = placeMark.locality {
                    // Country
                    if let country = placeMark.country {
                        print(country)

                        let currentLocationCity = City.init(name: cityName, country: country, selected: true, isCurrentLocation: true)
                        
                        if self.cities.contains(currentLocationCity) {
                            
                            self.locationManager.stopUpdatingLocation()
                            
                            return
                        }
                        
                        CitiesManager.shared.replaceCurrentLocationCity(with: currentLocationCity)

                        self.loadCities()
                        
                        self.locationManager.stopUpdatingLocation()
                        
                        SVProgressHUD.dismiss()

                    }
                }

        })
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
  
            SVProgressHUD.dismiss()

            if self.cities.count == 0 {
                self.navigationController?.setViewControllers([MyCitiesViewController(), SelectCityViewController()], animated: true)
            }
        }
    }
}
