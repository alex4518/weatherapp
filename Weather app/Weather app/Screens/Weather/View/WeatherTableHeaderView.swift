//
//  WeatherTableHeaderView.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class WeatherTableHeaderView: UIView {
    
    //MARK: constraints constants
    var citylabelTop: CGFloat!
    var citylabelFont: CGFloat!
    var locationIconLeft: CGFloat!
    var locationIconWidth: CGFloat!
    var locationIconHeight: CGFloat!
    var weatherViewTop: CGFloat!
    var weatherViewLeft: CGFloat!
    var weatherViewRight: CGFloat!
    var weatherViewBottom: CGFloat!
    var weatherViewHeight: CGFloat!
    var weatherViewCornerRadius: CGFloat!
    var feelTempLabelFontSize: CGFloat!
    var feelTempLabelTop: CGFloat!
    var tempLabelFont: CGFloat!
    var tempLabelBottom: CGFloat!
    var descLabelFont: CGFloat!
    var descLabelTop: CGFloat!
    var windLabelFont: CGFloat!
    var windLabelTop: CGFloat!
    var minLabelFont: CGFloat!
    var minLabelCenterY: CGFloat!
    var minLabelLeft: CGFloat!
    var maxLabelFont: CGFloat!
    var maxLabelCenterY: CGFloat!
    var maxLabelRight: CGFloat!
    var humidityLabelFont: CGFloat!
    var humidityLabelCenterY: CGFloat!
    var humidityLabelLeft: CGFloat!
    var visibilityLabelFont: CGFloat!
    var visibilityLabelCenterY: CGFloat!
    var visibilityLabelRight: CGFloat!
    
    //MARK: UI vars
    var weatherView = UIView()
    var cityLabel = UILabel()
    var locationIcon = UIImageView()
    var weatherIcon = UIImageView()
    var tempLabel = UILabel()
    var feelsLikeLabel = UILabel()
    var descLabel = UILabel()
    var windLabel = UILabel()
    var minTempLabel = UILabel()
    var maxTempLabel = UILabel()
    var humidityLabel = UILabel()
    var visibilityLabel = UILabel()
  
    convenience init(isIpad: Bool) {
        self.init(frame: .zero)
        
        self.setConstants(isIpad: isIpad)

        self.addCityLabel()
        self.addLocationIcon()
        self.addWeatherView()
        self.addWeatherIcon()
        self.addFeelsLikeLabel()
        self.addDescLabel()
        self.addWindLabel()
        self.addTempLabel()
        self.addMinTempLabel()
        self.addMaxTempLabel()
        self.addVisibilityLabel()
        self.addHumidityLabel()
        
    }
    
    //MARK: Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Constants configuration
    
    private func setConstants(isIpad: Bool) {
        self.citylabelTop = isIpad ? 30 : 20
        self.citylabelFont = isIpad ? 24 : 16
        self.locationIconLeft = isIpad ? 52 : 35
        self.locationIconWidth = isIpad ? 52 : 35
        self.locationIconHeight = isIpad ? 52 : 35
        self.weatherViewTop = isIpad ? 60 : 40
        self.weatherViewLeft = isIpad ? 30 : 20
        self.weatherViewRight = isIpad ? -30 : -20
        self.weatherViewBottom = isIpad ? -75 : -50
        self.weatherViewHeight = isIpad ? 300 : 200
        self.weatherViewCornerRadius = isIpad ? 7 : 5
        self.feelTempLabelFontSize = isIpad ? 15 : 10
        self.feelTempLabelTop = isIpad ? -7 : -5
        self.tempLabelFont = isIpad ? 60 : 40
        self.tempLabelBottom = isIpad ? -3 : -2
        self.descLabelFont = isIpad ? 27 : 18
        self.descLabelTop = isIpad ? 15 : 10
        self.windLabelFont = isIpad ? 18 : 12
        self.windLabelTop = isIpad ? 7 : 5
        self.minLabelFont = isIpad ? 24 : 16
        self.minLabelCenterY = isIpad ? -45 : -30
        self.minLabelLeft = isIpad ? 15 : 10
        self.maxLabelFont = isIpad ? 24 : 16
        self.maxLabelCenterY = isIpad ? -45 : -30
        self.maxLabelRight = isIpad ? -15 : -10
        self.humidityLabelFont = isIpad ? 24 : 16
        self.humidityLabelCenterY = isIpad ? 45 : 30
        self.humidityLabelLeft = isIpad ? 15 : 10
        self.visibilityLabelFont = isIpad ? 24 : 16
        self.visibilityLabelCenterY = isIpad ? 45 : 30
        self.visibilityLabelRight = isIpad ? -15 : -10
    }
    
    //MARK: UI
    
    private func addCityLabel() {
        self.cityLabel.translatesAutoresizingMaskIntoConstraints = false
        self.cityLabel.textColor = UIColor.white
        self.cityLabel.font = UIFont.systemFont(ofSize: self.citylabelFont)
        
        self.addSubview(self.cityLabel)
        
        self.cityLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: self.citylabelTop).isActive = true
        self.cityLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    private func addLocationIcon() {
        self.locationIcon.translatesAutoresizingMaskIntoConstraints = false
        self.locationIcon.image = UIImage.init(named: "globe_location")
        
        self.addSubview(self.locationIcon)
        
        self.locationIcon.centerYAnchor.constraint(equalTo: self.cityLabel.centerYAnchor).isActive = true
        self.locationIcon.leftAnchor.constraint(equalTo: self.cityLabel.rightAnchor, constant: self.locationIconLeft).isActive = true
        self.locationIcon.widthAnchor.constraint(equalToConstant: self.locationIconHeight).isActive = true
        self.locationIcon.heightAnchor.constraint(equalToConstant: self.locationIconWidth).isActive = true

    }
    
    private func addWeatherView() {
        self.weatherView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.weatherView)
        
        self.weatherView.backgroundColor = UIColor.weatherViewBackground
        self.weatherView.layer.cornerRadius = self.weatherViewCornerRadius
        
        self.weatherView.topAnchor.constraint(equalTo: self.cityLabel.bottomAnchor, constant: self.weatherViewTop).isActive = true
        self.weatherView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.weatherViewLeft).isActive = true
        self.weatherView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: self.weatherViewRight).isActive = true
        self.weatherView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: self.weatherViewBottom).isActive = true
        
        self.weatherView.heightAnchor.constraint(equalToConstant: self.weatherViewHeight).isActive = true
    }
    
    private func addWeatherIcon() {
        self.weatherIcon.translatesAutoresizingMaskIntoConstraints = false
        
        self.weatherView.addSubview(self.weatherIcon)
        
        self.weatherIcon.centerXAnchor.constraint(equalTo: self.weatherView.centerXAnchor).isActive = true
        self.weatherIcon.centerYAnchor.constraint(equalTo: self.weatherView.centerYAnchor).isActive = true
    }
    
    private func addFeelsLikeLabel() {
        self.feelsLikeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.feelsLikeLabel.textColor = UIColor.white
        self.feelsLikeLabel.font = UIFont.systemFont(ofSize: self.feelTempLabelFontSize)
        
        self.weatherView.addSubview(self.feelsLikeLabel)
        
        self.feelsLikeLabel.bottomAnchor.constraint(equalTo: self.weatherIcon.topAnchor, constant: self.feelTempLabelTop).isActive = true
        self.feelsLikeLabel.centerXAnchor.constraint(equalTo: self.weatherView.centerXAnchor).isActive = true
    }
    
    private func addTempLabel() {
        self.tempLabel.translatesAutoresizingMaskIntoConstraints = false
        self.tempLabel.textColor = UIColor.white
        self.tempLabel.font = UIFont.systemFont(ofSize: self.tempLabelFont)

        self.weatherView.addSubview(self.tempLabel)
        
        self.tempLabel.bottomAnchor.constraint(equalTo: self.feelsLikeLabel.topAnchor, constant: self.tempLabelBottom).isActive = true
        self.tempLabel.centerXAnchor.constraint(equalTo: self.weatherView.centerXAnchor).isActive = true
    }
    
    private func addDescLabel() {
        self.descLabel.translatesAutoresizingMaskIntoConstraints = false
        self.descLabel.textColor = UIColor.white
        self.descLabel.font = UIFont.systemFont(ofSize: self.descLabelFont)
        
        self.weatherView.addSubview(self.descLabel)
        
        self.descLabel.topAnchor.constraint(equalTo: self.weatherIcon.bottomAnchor, constant: self.descLabelTop).isActive = true
        self.descLabel.centerXAnchor.constraint(equalTo: self.weatherView.centerXAnchor).isActive = true
    }
    
    private func addWindLabel() {
        self.windLabel.translatesAutoresizingMaskIntoConstraints = false
        self.windLabel.textColor = UIColor.white
        self.windLabel.font = UIFont.systemFont(ofSize: self.windLabelFont)
        
        self.weatherView.addSubview(self.windLabel)
        
        self.windLabel.topAnchor.constraint(equalTo: self.descLabel.bottomAnchor, constant: self.windLabelTop).isActive = true
        self.windLabel.centerXAnchor.constraint(equalTo: self.weatherView.centerXAnchor).isActive = true
    }
    
    func addMinTempLabel() {
        self.minTempLabel.translatesAutoresizingMaskIntoConstraints = false
        self.minTempLabel.textColor = UIColor.white
        self.minTempLabel.font = UIFont.systemFont(ofSize: self.minLabelFont)
        
        self.weatherView.addSubview(self.minTempLabel)
        
        self.minTempLabel.centerYAnchor.constraint(equalTo: self.weatherIcon.centerYAnchor, constant: self.minLabelCenterY).isActive = true
        self.minTempLabel.leftAnchor.constraint(equalTo: self.weatherView.leftAnchor, constant: self.minLabelLeft).isActive = true
    }
    
    func addMaxTempLabel() {
        self.maxTempLabel.translatesAutoresizingMaskIntoConstraints = false
        self.maxTempLabel.textColor = UIColor.white
        self.maxTempLabel.font = UIFont.systemFont(ofSize: self.maxLabelFont)

        self.weatherView.addSubview(self.maxTempLabel)
        
        self.maxTempLabel.centerYAnchor.constraint(equalTo: self.weatherIcon.centerYAnchor, constant: self.maxLabelCenterY).isActive = true
        self.maxTempLabel.rightAnchor.constraint(equalTo: self.weatherView.rightAnchor, constant: self.maxLabelRight).isActive = true
    }
    
    func addHumidityLabel() {
        self.humidityLabel.translatesAutoresizingMaskIntoConstraints = false
        self.humidityLabel.textColor = UIColor.white
        self.humidityLabel.numberOfLines = 0
        self.humidityLabel.textAlignment = .center
        self.humidityLabel.font = UIFont.systemFont(ofSize: self.humidityLabelFont)

        self.weatherView.addSubview(self.humidityLabel)
        
        self.humidityLabel.centerYAnchor.constraint(equalTo: self.weatherIcon.centerYAnchor, constant: self.humidityLabelCenterY).isActive = true
        self.humidityLabel.leftAnchor.constraint(equalTo: self.weatherView.leftAnchor, constant: self.humidityLabelLeft).isActive = true
    }
    
    func addVisibilityLabel() {
        self.visibilityLabel.translatesAutoresizingMaskIntoConstraints = false
        self.visibilityLabel.textColor = UIColor.white
        self.visibilityLabel.numberOfLines = 0
        self.visibilityLabel.textAlignment = .center
        self.visibilityLabel.font = UIFont.systemFont(ofSize: self.visibilityLabelFont)

        self.weatherView.addSubview(self.visibilityLabel)
        
        self.visibilityLabel.centerYAnchor.constraint(equalTo: self.weatherIcon.centerYAnchor, constant: self.visibilityLabelCenterY).isActive = true
        self.visibilityLabel.rightAnchor.constraint(equalTo: self.weatherView.rightAnchor, constant: self.visibilityLabelRight).isActive = true
    }
}
