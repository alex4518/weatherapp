//
//  WeatherButton.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import SDWebImage

class WeatherButton: UIButton {

    //MARK: Button type
    enum WeatherButtonType {
        case today
        case longTerm
    }
    
    //MARK: constraints constants
    
    var iconTop: CGFloat!
    var iconLeft: CGFloat!
    var iconRight: CGFloat!
    var iconWidth: CGFloat!
    var iconHeight: CGFloat!
    var textViewCornerRadius: CGFloat!
    var texViewBorderWidth: CGFloat!
    var textViewFont: CGFloat!
    var textViewTop: CGFloat!
    var tempLabelFont: CGFloat!
    var tempLabelTop: CGFloat!
    var tempLabelBottom: CGFloat!
    
    //MARK: UI vars
    var icon = UIImageView()
    var timeTextView =  UITextView()
    var tempLabel = UILabel()
    var index: Int!
    var type: WeatherButtonType!
    
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.timeTextView.backgroundColor = UIColor.white
                self.timeTextView.layer.borderColor = UIColor.darkGray.cgColor
                self.timeTextView.textColor = UIColor.black
            }
            else {
                self.timeTextView.backgroundColor = UIColor.clear
                self.timeTextView.layer.borderColor = UIColor.clear.cgColor
                self.timeTextView.textColor = UIColor.white
            }
        }
    }

    convenience init(isIpad: Bool) {
        self.init(frame: .zero)
        
        self.setConstants(isIpad: isIpad)
        
        self.addIcon()
        self.addTimeLabel()
        self.addTempLabel()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func setConstants(isIpad: Bool) {
        self.iconTop = isIpad ? 15 : 10
        self.iconLeft = isIpad ? 30 : 20
        self.iconRight = isIpad ? -30 : -20
        self.iconWidth = isIpad ? 75 : 50
        self.iconHeight = isIpad ? 75 : 50
        self.textViewCornerRadius = isIpad ? 7 : 5
        self.texViewBorderWidth = isIpad ? 1.5 : 1
        self.textViewFont = isIpad ? 18 : 12
        self.textViewTop = isIpad ? 5 : 3
        self.tempLabelFont = isIpad ? 18 : 12
        self.tempLabelTop = isIpad ? 5 : 3
        self.tempLabelBottom = isIpad ? -5 : -3
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: UI
    
    private func addIcon() {
        self.icon.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.icon)
        
        self.icon.topAnchor.constraint(equalTo: self.topAnchor, constant: self.iconTop).isActive = true
        self.icon.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.iconLeft).isActive = true
        self.icon.rightAnchor.constraint(equalTo: self.rightAnchor, constant: self.iconRight).isActive = true
        self.icon.widthAnchor.constraint(equalToConstant: self.iconWidth).isActive = true
        self.icon.heightAnchor.constraint(equalToConstant: self.iconHeight).isActive = true
    }
    
    private func addTimeLabel() {
        self.timeTextView.translatesAutoresizingMaskIntoConstraints = false
        self.timeTextView.textColor = UIColor.white
        self.timeTextView.layer.cornerRadius = self.textViewCornerRadius
        self.timeTextView.layer.borderWidth = self.texViewBorderWidth
        self.timeTextView.font = UIFont.systemFont(ofSize: self.textViewFont)
        self.timeTextView.textContainerInset = UIEdgeInsets(top: 8, left: 6, bottom: 9, right: 6);
        self.timeTextView.isScrollEnabled = false
        self.timeTextView.isEditable = false
        self.timeTextView.isUserInteractionEnabled = false //do not allow to steal touch from button
        
        self.addSubview(self.timeTextView)
        
        self.timeTextView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.timeTextView.topAnchor.constraint(equalTo: self.icon.bottomAnchor, constant: self.textViewTop).isActive = true
    }
    
    private func addTempLabel() {
        self.tempLabel.translatesAutoresizingMaskIntoConstraints = false
        self.tempLabel.textColor = UIColor.white
        self.tempLabel.font = UIFont.systemFont(ofSize: self.tempLabelFont)

        self.addSubview(self.tempLabel)
        
        self.tempLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.tempLabel.topAnchor.constraint(equalTo: self.timeTextView.bottomAnchor, constant: self.tempLabelTop).isActive = true
        self.tempLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: self.tempLabelBottom).isActive = true
    }
}
