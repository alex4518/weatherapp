//
//  WeatherTableViewCell.swift
//  Weather app
//
//  Created by Alexandros Albanis on 14/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    var scrollView = UIScrollView()
    var stackView = UIStackView()
    
    //MARK: Lifecycle methods

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.weatherCellBackground
        self.addScrollView()
        self.addStackView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: UI
    
    func addScrollView() {
        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.scrollView)
        
        self.scrollView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        self.scrollView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        self.scrollView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        self.scrollView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        self.scrollView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor).isActive = true

    }
    
    func addStackView() {
        self.stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.stackView.axis = .horizontal
        self.stackView.spacing = 0
        
        self.scrollView.addSubview(self.stackView)
        
        self.stackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor).isActive = true
        self.stackView.leftAnchor.constraint(equalTo: self.scrollView.leftAnchor).isActive = true
        self.stackView.rightAnchor.constraint(equalTo: self.scrollView.rightAnchor).isActive = true
        self.stackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor).isActive = true
        self.stackView.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor).isActive = true
    }

}
