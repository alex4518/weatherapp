//
//  SelectCityViewController.swift
//  Weather app
//
//  Created by Alexandros Albanis on 11/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import CoreData

class SelectCityViewController: UIViewController {

    private var tableView = UITableView()
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var cities = [City]()
    private var filteredCities = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.citiesViewBackground
        
        self.loadCities()
        self.addTableView()
        self.setupSearchController()
    }
    
    //MARK: UI
    private func addTableView() {
        self.view.addSubview(self.tableView)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.tableView.register(CityTableViewCell.self, forCellReuseIdentifier: "cityCell")
        self.tableView.rowHeight = UITableView.automaticDimension

        
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    private func setupSearchController() {
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search Cities"
        self.searchController.searchBar.delegate = self
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true //make sure the search bar does not remain on the screen if the user navigates to another view controller
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        self.searchController.searchBar.tintColor = UIColor.white
        
        self.searchController.searchBar.barStyle = .black

    }
    
    private func loadCities() {
        let parser = CSVParser.init();
        if let string = parser.readDataFromCSV(fileName: "world-cities", fileType: "csv") {
            self.cities = parser.parseData(data: string);
        }
        
//        if the csv file cannot be read the user can just search by typing the exact name of the city
    }
    
    private func saveToCoreData(city: City) {
        CitiesManager.shared.saveToCoreData(city: city)
    }
    
    //MARK: Search table
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        self.filteredCities = self.cities.filter({( city : City) -> Bool in
            return city.name.lowercased().contains(searchText.lowercased())
        })
        
        self.tableView.reloadData()
    }
    
    private func isFiltering() -> Bool {
        return self.searchController.isActive && !self.searchBarIsEmpty()
    }
}

extension SelectCityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFiltering() {
            return self.filteredCities.count
        }
        
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let city: City
        if self.isFiltering() {
            city = self.filteredCities[indexPath.row]
        }
        else {
            city = self.cities[indexPath.row]
        }
        
        self.saveToCoreData(city: city)
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectCityViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cityCell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as? CityTableViewCell
        
        let city: City
        if self.isFiltering() {
            city = self.filteredCities[indexPath.row]
        } else {
            city = self.cities[indexPath.row]
        }
        cityCell?.textLabel?.text = city.name + ", " + city.country
        
        return cityCell ?? UITableViewCell();
    }
}

extension SelectCityViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContentForSearchText(self.searchController.searchBar.text!)

    }
}

extension SelectCityViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        self.getWeather(location: searchBar.text)
    }
}

//MARK: Api call
extension SelectCityViewController {
    private func getWeather(location: String? ) {
        guard location != nil else {
            return
        }
        do {
            try APIClient.getWeather(city: location!, days: 5, success: { response in
                
                if (response.data?.hasError())! {
                    AlertManager.shared.showAlert(title: nil, message: (response.data?.getErrorMsg())!, buttonTitle: "Ok", vc: self, handler: nil)

                    return
                }
                
                let fullCityName = response.data?.request?.first?.query
                if let components = fullCityName?.split(separator: ",") {
                
                    let city = City.init(name: String(components.first!), country: String(components[1]), selected: true, isCurrentLocation: false)
                    
                    CitiesManager.shared.saveToCoreData(city: city)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
                
            }, error: { error in
                
                var errorMessage = ""
                switch(error) {
                case .noNetworkError:
                    errorMessage = "No internet connection found. Please connect to the internet."
                case .apiError:
                    fallthrough
                case .timeoutError:
                    fallthrough
                case .malformedRequest:
                    fallthrough
                default:
                    errorMessage = "Something went wrong. Please try again later"
                }
                
                AlertManager.shared.showAlert(title: nil, message: errorMessage, buttonTitle: "Ok", vc: self, handler: nil)
            })
        }
        catch (_) {
            AlertManager.shared.showAlert(title: nil, message: "Something went wrong. Please try again later", buttonTitle: "Ok", vc: self, handler: nil)
        }
    }
}
