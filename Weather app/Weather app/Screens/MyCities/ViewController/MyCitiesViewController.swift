//
//  MyCitiesViewController.swift
//  Weather app
//
//  Created by Alexandros Albanis on 12/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit
import CoreData

class MyCitiesViewController: UIViewController {

    private var tableView = UITableView()
    private var cities = [City]()

    //MARK: Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.citiesViewBackground
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addButtonPressed))
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.leftBarButtonItem = addButton

        self.addTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadCities()
        self.tableView.reloadData()
    }
    
    @objc private func addButtonPressed() {
        self.navigationController?.pushViewController(SelectCityViewController.init(), animated: true)
    }
    
    //MARK: Cell editing
    
    override func setEditing (_ editing:Bool, animated:Bool) {
        super.setEditing(editing,animated:animated)
        self.tableView.setEditing(editing, animated: true)

    }
    
    //MARK: Data binding
    private func loadCities() {
        if let cities = CitiesManager.shared.loadCitiesFromCoreData() {
            self.cities = cities
        }
        else {
            AlertManager.shared.showAlert(title: nil, message: "We couldn't load your cities. Please try again later.", buttonTitle: "Ok", vc: self, handler: nil)
        }
    }
}

//MARK UI
extension MyCitiesViewController {
    private func addTableView() {
        self.view.addSubview(self.tableView)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        self.tableView.register(CityTableViewCell.self, forCellReuseIdentifier: "cityCell")
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60
        
        self.tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.tableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
}

extension MyCitiesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCity = self.cities[indexPath.row]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cities")
        request.returnsObjectsAsFaults = false
        
        //find the city in core data and set it selected so that the next time the app launches the weather for this city will be shown
        do {
            let result = try context.fetch(request)
            for (coreDataCity) in result as! [NSManagedObject] {
                
                if let name = coreDataCity.value(forKey: "name") as? String, let country = coreDataCity.value(forKey: "country") as? String, let selected = coreDataCity.value(forKey: "selected") as? Bool, let isCurrentLocation = coreDataCity.value(forKey: "isCurrentLocation") as? Bool {
                    
                    let storedCity = City.init(name: name, country: country, selected: selected, isCurrentLocation: isCurrentLocation)
                    if selectedCity == storedCity {
                        coreDataCity.setValue(true, forKey: "selected")
                    }
                    else {
                        coreDataCity.setValue(false, forKey: "selected")
                    }
                        
                    do {
                        try context.save()
                    } catch {
                        //handle error silently
                    }
                }
            }
            
        } catch {
            //handle error silently
        }
                
        self.navigationController?.setViewControllers([WeatherViewController()], animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let city = self.cities[indexPath.row]
        if city.isCurrentLocation {
            return false
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            if CitiesManager.shared.remove(city: self.cities[indexPath.row]) {
                self.cities.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            else {
                AlertManager.shared.showAlert(title: nil, message: "Something went wrong. Please try again later.", buttonTitle: "Ok", vc: self, handler: nil)
            }
        }
    }
    
    //use a plain view as footer to avoid showing empty cells
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension MyCitiesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cityCell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as? CityTableViewCell
        
        let city = self.cities[indexPath.row]

        cityCell?.textLabel?.text = city.name + ", " + city.country
        
        if city.isCurrentLocation {
            cityCell?.imageView?.image = UIImage.init(named: "globe_location")
        }
        else {
            cityCell?.imageView?.image = nil

        }
        
        return cityCell ?? UITableViewCell();
    }
}
