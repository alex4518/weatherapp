//
//  CityTableViewCell.swift
//  Weather app
//
//  Created by Alexandros Albanis on 20/04/2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    //MARK: Lifecycle methods

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.cityCellBackground
        
        self.textLabel?.textColor = UIColor.white
        
        self.contentView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
